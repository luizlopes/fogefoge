def da_boas_vindas
  puts "Bem vindo ao foge-foge"
  puts "Qual o seu nome?"
  nome = gets.strip
  puts "Começaremos o jogo para você, #{nome}"
  nome
end

def desenha(mapa)
  puts "\n\n\n\n\n\n"
  puts mapa
  puts "\n\n\n\n\n\n\n"
end

def pede_movimento
  gets.strip.upcase
end

def game_over
  puts "\n\n\n\n\n\n"
  puts "GAME OVER"
end
