require_relative 'ui'
require_relative 'heroi'

def le_mapa(numero)
  arquivo = "mapa#{numero}.txt"
  texto = File.read arquivo
  mapa = texto.split "\n"
end

def encontra_jogador(mapa)
  caracter_do_heroi = "H"
  mapa.each_with_index do |linha_atual, linha|
    coluna_do_heroi = linha_atual.index(caracter_do_heroi)
    if coluna_do_heroi
      jogador = Heroi.new
      jogador.linha = linha
      jogador.coluna = coluna_do_heroi
      return jogador
    end
  end
  nil
end

def nao_existe_posicao?(mapa, posicao)
  linhas = mapa.size
  colunas = mapa[0].size
  estourou_linhas = posicao[0] < 0 || posicao[0] >= linhas 
  estourou_colunas = posicao[1] < 0 || posicao[1] >= colunas
  
  nao_existe_posicao = estourou_linhas || estourou_colunas
end

def posicao_valida?(mapa, posicao)
  if nao_existe_posicao?(mapa, posicao)
    return false
  end
  
  posicao_atual = mapa[posicao[0]][posicao[1]]
  if posicao_atual == "X" || posicao_atual == "F"
    return false
  end
  true
end

def posicoes_validas?(mapa1, mapa2, posicao)
  validas = posicao_valida?(mapa1, posicao) && posicao_valida?(mapa2, posicao)
end

def posicao_eh_muro?(mapa, posicao)
  mapa[posicao.linha][posicao.coluna] == "X"
end

def executa_remocao(mapa, posicao, quantidade)
  if nao_existe_posicao?(mapa, posicao.to_array) || (posicao_eh_muro?(mapa, posicao))
    return
  end
  posicao.remove_do mapa
  remove mapa, posicao, quantidade -1
end

def remove(mapa, posicao, quantidade)
  return if quantidade == 0
  executa_remocao mapa, posicao.direita, quantidade
  executa_remocao mapa, posicao.cima, quantidade
  executa_remocao mapa, posicao.esquerda, quantidade
  executa_remocao mapa, posicao.baixo, quantidade
end

def anda_com_heroi(mapa, heroi, direcao)
  heroi_nova_posicao = heroi.calcula_nova_posicao direcao
  if posicao_valida? mapa, heroi_nova_posicao.to_array
    heroi.remove_do mapa
    if mapa[heroi_nova_posicao.linha][heroi_nova_posicao.coluna] == "*"
      remove mapa, heroi_nova_posicao, 4
    end
    heroi_nova_posicao.coloca_no mapa
  end
  mapa
end

def direcao_valida?(direcao)
  direcao_nao_unica = direcao.size != 1
  if direcao_nao_unica
    return false
  end
  
  direcoes = "WASD"
  if direcoes.include? direcao
    return true
  end
  false
end

def filtra_posicoes_validas(mapa1, mapa2, posicoes)
  posicoes_validas = []
  posicoes.each do |posicao|
    if posicoes_validas?(mapa1, mapa2, posicao)
      posicoes_validas << posicao
    end
  end
  posicoes_validas
end

def posicoes_validas_para(mapa, novo_mapa, posicao)
  baixo = [posicao[0] + 1, posicao[1]]
  direita = [posicao[0], posicao[1] + 1]
  cima = [posicao[0] - 1, posicao[1]]
  esquerda = [posicao[0], posicao[1] - 1]
  
  posicoes = [baixo, direita, cima, esquerda]
  posicoes_validas = filtra_posicoes_validas(mapa, novo_mapa, posicoes)
end

def limpa_fantasmas_do_mapa(mapa)
  novo_mapa = mapa.join("\n").tr("F", " ").split "\n"
end

def move_fantasma(mapa, novo_mapa, linha, coluna)
  posicoes = posicoes_validas_para(mapa, novo_mapa, [linha, coluna])  
  if posicoes == nil
    return
  end
  movimento_aleatorio = rand posicoes.size
  posicao = posicoes[movimento_aleatorio]
  if posicao_valida? mapa, posicao
    mapa[linha][coluna] = " "
    novo_mapa[posicao[0]][posicao[1]] = "F"
  end
end

def move_fantasmas(mapa)
  caractere_do_fantasma = "F"
  novo_mapa = limpa_fantasmas_do_mapa mapa
  mapa.each_with_index do |linha_atual, linha|
    linha_atual.chars.each_with_index do |caractere_atual, coluna|
      eh_fantasma = caractere_atual == caractere_do_fantasma
      if eh_fantasma
        move_fantasma mapa, novo_mapa, linha, coluna
      end
    end
  end
  novo_mapa
end

def jogador_morreu?(mapa)
  jogador_morreu = encontra_jogador(mapa) == nil
end

def joga(nome)
  mapa = le_mapa 3
  while true
    desenha mapa
    direcao = pede_movimento
    if not direcao_valida? direcao
      next
    end
    heroi = encontra_jogador mapa
    mapa = anda_com_heroi mapa, heroi, direcao
    mapa = move_fantasmas mapa
    if jogador_morreu? mapa
      game_over
      break
    end
  end
end

def inicia_fogefoge
  nome = da_boas_vindas
  joga nome
end
